const Discord = require('discord.js');
const auth = require('./auth.json');
const client = new Discord.Client();
let fs = require('fs');
const exec = require('child_process').exec;

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});


function download(uri, filename, callback){
    let request = require('request');

    request.head(uri, function(err, res, body){
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};
function os_func() {
    this.execCommand = function(cmd, callback) {
        exec(cmd, (error, stdout, stderr) => {
            if (error) {
                console.error(`exec error: ${error}`);
                return;
            }

            callback(stdout);
        });
    }
}

client.on('message', msg => {
    if(msg.author.id != client.user.id){
        if(msg.guild.id === '503294158811824129' || msg.channel.id === '846496495477850132' || msg.channel.type === 'dm' || msg.channel.id === '849247448739741706'){
            let args = msg.toString().toLowerCase().substring(0).split(' ');
            let cmd = args[0];

            try{
                switch(cmd) {
                    case '>help':
                        msg.channel.send(':grey_question: Decensoring tutorial\n\n***It takes roughly 3min to decensor a single image!***\n\n**1.** Send a `PNG`, `JPG` or `JPEG` censored image to this channel with `mosaic` or `bar` as the image message (depending on the censor type).\n**2.** Just wait for the AI to decensor it, the result will be sent back here (you will be pinged as well).\n\n*Mosaic censored images are harder to decensor for the AI. The results varies a lot for each image, but often end up badly decensored.*\n*The AI cannot deal with extreme censoring, or censor that covers the entire nsfw part.*');
                    break;
                    case 'mosaic':
                        msg.channel.send(':x: Mosaic decensoring is currently on hold, sorry.');
                    break;
                    case 'bar':
                        let variation = 1;
                        let full = false;

                        if(args[1] != undefined && args[1] == 'full'){
                            full = true;
                        }

                        if(args[2] != undefined && args[2] != "" && !isNaN(args[2])){
                            variation = args[2];
                        }
                        if(variation > 4){
                            variation = 4;
                        }
                        if(variation < 1){
                            variation = 1;
                        }
                        if(variation == 3){
                            variation = 2;
                        }

                        msg.attachments.each(attachment => {
                            if(!attachment.name.includes(".png") && !attachment.name.includes(".jpg") && !attachment.name.includes(".jpeg")){
                                msg.channel.send(':warning: Only `PNG`, `JPG` and `JPEG` files are accepted. Please convert your image first.');
                            }
                            else{
                                pngname = attachment.name.replace('.jpg', '.png').replace('.jpeg', '.png');
                                resname = pngname.replace('.png', ' 0.png');

                                msg.channel.send(':white_check_mark: Image detected, downloading...');
                                download(attachment.url, '/home/eidenz/hent-AI/input/' + attachment.name, function(){
                                    //finished downloading
                                    if(fs.existsSync('/home/eidenz/hent-AI/input/' + attachment.name)){
                                        msg.channel.send(':white_check_mark: Successfully downloaded. Starting censor detection...');
                                        console.log('[hent-AI] detection started.');

                                        let os = new os_func();

                                        os.execCommand('/home/eidenz/hent-AI/' + cmd + '.sh', function (hentai) {
                                            if(fs.existsSync('/home/eidenz/hent-AI/DCP/decensor_input/' + pngname)){
                                                if(full){
                                                    msg.channel.send('User asked for full process to be shown, here is the censor detection image.', {
                                                        files: [
                                                            '/home/eidenz/hent-AI/DCP/decensor_input/' + pngname
                                                        ]
                                                    });
                                                }

                                                files = [
                                                    "/home/eidenz/hent-AI/DCP/decensor_output/" + resname
                                                ];

                                                if(variation !== 1){
                                                    for(let i=1;i<variation;i++){
                                                        files.push("/home/eidenz/hent-AI/DCP/decensor_output/" + resname.replace('0.png', i + '.png'));
                                                    }
                                                    msg.channel.send(':white_check_mark: Censor detection finished. Starting decensoring process with ' + variation + ' variations...');
                                                    console.log('[hent-AI] decensoring started. User asked for ' + variation + ' variations.');
                                                }
                                                else{
                                                    msg.channel.send(':white_check_mark: Censor detection finished. Starting decensoring process...');
                                                    console.log('[hent-AI] decensoring started.');
                                                }
                                        
                                                os.execCommand('/home/eidenz/hent-AI/DCP/' + cmd + '.sh ' + variation, function (dcp) {
                                                    console.log('[hent-AI] decensoring finished.');
                                                    msg.channel.send(':white_check_mark: Decensoring process finished, <@' + msg.author.id + '>.', {
                                                        files
                                                    })
                                                    .then(message => {
                                                        //removing files
                                                        try{
                                                            fs.unlinkSync('/home/eidenz/hent-AI/input/' + attachment.name);
                                                            fs.unlinkSync('/home/eidenz/hent-AI/DCP/decensor_input/' + pngname);
                                                            fs.unlinkSync('/home/eidenz/hent-AI/DCP/decensor_output/' + resname);
                                                            fs.unlinkSync('/home/eidenz/hent-AI/DCP/decensor_input_original/' + attachment.name);
                                                        }catch(err){}
                                                    })
                                                    .catch(error => {
                                                        console.error('[hent-AI] decensoring failed: ', error);
                                                        msg.channel.send(':x: An error occured while decensoring your image. Please try again.');
                                                        //removing files
                                                        try{
                                                            fs.unlinkSync('/home/eidenz/hent-AI/input/' + attachment.name);
                                                            fs.unlinkSync('/home/eidenz/hent-AI/DCP/decensor_input/' + pngname);
                                                            fs.unlinkSync('/home/eidenz/hent-AI/DCP/decensor_output/' + resname);
                                                            fs.unlinkSync('/home/eidenz/hent-AI/DCP/decensor_input_original/' + attachment.name);
                                                        }catch(err){}
                                                    });
                                                });
                                            }
                                            else{
                                                console.log('[hent-AI] detection failed.');
                                                msg.channel.send(':x: An error occured while detecting censored parts. Please try again.');
                                                try{
                                                    fs.unlinkSync('/home/eidenz/hent-AI/input/' + attachment.name);
                                                    fs.unlinkSync('/home/eidenz/hent-AI/DCP/decensor_input/' + pngname);
                                                    fs.unlinkSync('/home/eidenz/hent-AI/DCP/decensor_input_original/' + attachment.name);
                                                }catch(err){}
                                            }
                                        });
                                    }
                                    else{
                                        msg.channel.send(':x: An error occured while downloading your image. Please try again.');
                                        try{
                                            fs.unlinkSync('/home/eidenz/hent-AI/input/' + attachment.name);
                                            fs.unlinkSync('/home/eidenz/hent-AI/DCP/decensor_input/' + pngname);
                                            fs.unlinkSync('/home/eidenz/hent-AI/DCP/decensor_input_original/' + attachment.name);
                                        }catch(err){}
                                    }
                                });
                            }
                        });
                    break;
                }
            }
            catch(err){
                console.error('Unknown error occured in command: ' + err.name + ' - ' + err.message);
            }
        }
    }
});

client.login(auth.token);
